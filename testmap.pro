#-------------------------------------------------
#
# Project created by QtCreator 2017-01-15T23:31:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = testmap
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    player.cpp \
    block.cpp

HEADERS  += mainwindow.h \
    player.h \
    block.h

FORMS    += mainwindow.ui

RESOURCES += \
    res.qrc
