#ifndef BLOCK_H
#define BLOCK_H

#include <QGraphicsPixmapItem>

class Block : public QGraphicsPixmapItem
{
public:
    Block();
    QPixmap image;

    // QGraphicsItem interface
public:
    QRectF boundingRect() const;
};

#endif // BLOCK_H
