#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    scene = new QGraphicsScene();
    item = new Player();



    ui->graphicsView->setScene(scene);
    scene->setSceneRect(ui->graphicsView->rect());


    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);




    timer = new QTimer();
timer->start(1000/600);

    scene->addItem(item);
    item->setPos(0,480-32-130);
    //item->setPos(0,200);

    drawMask();
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_D:
    {
        if(item->x() > 200 && stopItem_1X == 0)
        {
        ui->graphicsView->setGeometry(ui->graphicsView->x()-5,0,ui->graphicsView->width()+5,480);
        ui->graphicsView->scroll(-5,0);
        ui->graphicsView->setUpdatesEnabled(true);


        scene->setSceneRect(ui->graphicsView->rect());
        item->setX(item->x()+5);
        stopItem_1X = 0;

        item->runAnimation_D();

        }
        else
        {
            stopItem_1X = 0;
            item->setX(item->x()+5);
            item->runAnimation_D();
        }

        if(!scene->collidingItems(item).isEmpty())
        {
            item->setPos(item->x()-5,item->y());
            stopItem_1X = 1;
        }

    }
        break;
    case Qt::Key_A:
    {
        if(ui->graphicsView->x() <-1 && stopItem_2X == 0)
        {
        ui->graphicsView->setGeometry(ui->graphicsView->x()+5,0,ui->graphicsView->width()-5,480);
        ui->graphicsView->scroll(5,0);
        ui->graphicsView->setUpdatesEnabled(true);


        scene->setSceneRect(ui->graphicsView->rect());
        item->setX(item->x()-5);
        item->runAnimation_A();
        stopItem_2X = 0;
        }
        else
        {
            item->setX(item->x()-5);
            stopItem_2X = 0;
            item->runAnimation_A();
        }

        if(!scene->collidingItems(item).isEmpty())
        {
            item->setPos(item->x()+5,item->y());
            stopItem_2X = 1;
        }
    }break;
    default:
        break;
    }
}
void MainWindow::drawMask()
{
    for(int i(0);i<15;i++)
    {
        for(int j(0);j<21;j++)
        {
            if(map[i][j] == 'b')
            {
                block = new Block();

                scene->addItem(block);
                block->setPos(j*32,i*32);
            }
        }
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_D:
    {
    timer->stop();
    }break;

    default:
            break;
    }
}
