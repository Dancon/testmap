#ifndef PLAYER_H
#define PLAYER_H

#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QGraphicsScene>

class Player :public QObject,public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Player();
    QTimer *timer;

    QImage runFrame1;
    QImage runFrame2;
    QImage runFrame3;
    QImage runFrame4;
    QImage runFrame5;
    QImage runFrame6;

    QList<QPixmap> runSprite_D;
    QList<QPixmap> runSprite_A;

    int counterOfAnimation = 0;
    int frameCounter = 0;

public slots:
    void find();
    void runAnimation_D();
    void runAnimation_A();


    // QGraphicsItem interface
public:
    QRectF boundingRect() const;

};

#endif // PLAYER_H
