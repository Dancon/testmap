#include "player.h"
#include <QDebug>

Player::Player() : QObject(), QGraphicsPixmapItem()
{

    timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(find()));
    //timer->start(10);

    runFrame1.load(":/content/Run/Frame1.png");
    runFrame2.load(":/content/Run/Frame2.png");
    runFrame3.load(":/content/Run/Frame3.png");
    runFrame4.load(":/content/Run/Frame4.png");
    runFrame5.load(":/content/Run/Frame5.png");
    runFrame6.load(":/content/Run/Frame6.png");

    runSprite_D.append(QPixmap::fromImage(runFrame1));
    runSprite_D.append(QPixmap::fromImage(runFrame2));
    runSprite_D.append(QPixmap::fromImage(runFrame3));
    runSprite_D.append(QPixmap::fromImage(runFrame4));
    runSprite_D.append(QPixmap::fromImage(runFrame5));
    runSprite_D.append(QPixmap::fromImage(runFrame6));

    runFrame1 = runFrame1.mirrored(true,false);
    runFrame2 = runFrame2.mirrored(true,false);
    runFrame3 = runFrame3.mirrored(true,false);
    runFrame4 = runFrame4.mirrored(true,false);
    runFrame5 = runFrame5.mirrored(true,false);
    runFrame6 = runFrame6.mirrored(true,false);

    runSprite_A.append(QPixmap::fromImage(runFrame1));
    runSprite_A.append(QPixmap::fromImage(runFrame2));
    runSprite_A.append(QPixmap::fromImage(runFrame3));
    runSprite_A.append(QPixmap::fromImage(runFrame4));
    runSprite_A.append(QPixmap::fromImage(runFrame5));
    runSprite_A.append(QPixmap::fromImage(runFrame6));


    for(int i(0);i<6;i++)
    {

        runSprite_D[i].scaled(QSize(32,32),Qt::KeepAspectRatio);
        runSprite_A[i].scaled(QSize(32,32),Qt::KeepAspectRatio);
    }
    this->setPixmap(runSprite_D[0]);
}

void Player::find()
{
    QList<QGraphicsItem*>found = scene()->items(QPolygonF()
                                                    << mapToScene(100,0)
                                                    << mapToScene(105,10)
                                                    << mapToScene(100,20));
    //qDebug() << found;
    found.clear();


}

QRectF Player::boundingRect() const
{
    return QRectF(0,0,100,130);
}

void Player::runAnimation_D()
{
//    if(0frameCounter%40 == 0){
    if(frameCounter == 0)
    {
    if(counterOfAnimation == 5)
    {
        counterOfAnimation = 0;
        frameCounter = 1;
    }
        counterOfAnimation++;
    this->setPixmap(runSprite_D[counterOfAnimation]);
    }
    frameCounter++;
    if(frameCounter == 4)
        frameCounter = 0;
//    }
//    frameCounter++;
//    if(frameCounter == 1000)
    //        frameCounter = 0;
}

void Player::runAnimation_A()
{
    if(frameCounter == 0)
    {
    if(counterOfAnimation == 5)
    {
        counterOfAnimation = 0;
        frameCounter = 1;
    }
        counterOfAnimation++;
    this->setPixmap(runSprite_A[counterOfAnimation]);
    }
    frameCounter++;
    if(frameCounter == 4)
        frameCounter = 0;
}
