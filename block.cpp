#include "block.h"
#include <QDebug>

Block::Block() : QGraphicsPixmapItem()
{
    image.load(":/Tile.png");
    image = image.scaled(QSize(32,32),Qt::KeepAspectRatio);
    this->setPixmap(image);
}

QRectF Block::boundingRect() const
{
    return QRectF(0,0,32,32);
}
