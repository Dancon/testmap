#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QKeyEvent>
#include "player.h"
#include "block.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QGraphicsScene *scene;
    QPixmap image;
    QGraphicsPixmapItem *pixItem;
    QTimer *timer;


    Player *item;
    Block *block;
/////////////////////////////
    int coun= 0;
    bool isOnGround = true;
    int stopItem_1X = 0;
    int a = 0;
    int stopItem_2X  = 0;
////////////////////////////

    //s-sky;b-block
    QString map[15] = {
    "ssssssssssssssssssssb",
    "ssssssssssssssssssssb",
    "sssssssssssssssssssss",
    "ssssssssssssssssssssb",
    "ssssssssssssssssssssb",
    "ssssssssssssssssssssb",
    "ssssssssssssssssssssb",
    "ssssssssssssssssssssb",
    "ssssssssssssssssssssb",
    "ssssssssssssssssssssb",
    "ssssssssssssssssssssb",
    "ssssssssssssssssssssb",
    "sssssssssssssssssssb",
    "ssssssssssssssssssssb",
    "bbbbbbbbbbbbbbbbbbbbb",
  };
    void keyPressEvent(QKeyEvent *event);
public slots:
    void start();
private:
    Ui::MainWindow *ui;
    void drawMask();


    // QWidget interface
protected:
    virtual void keyReleaseEvent(QKeyEvent *event);
};

#endif // MAINWINDOW_H
